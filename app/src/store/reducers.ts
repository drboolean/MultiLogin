
import { BlockchainContext } from "./services/blockchainContext";



enum Theme {
  light = 0,
  dark = 1,
  alternative = 2
}

export interface IState {
 
  context: BlockchainContext;
  
}

const initialState: IState = {
  context: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
   
    default:
      return state;
  }
};

export { initialState, reducer };
