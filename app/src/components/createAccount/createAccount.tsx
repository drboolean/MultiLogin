import React, { useContext, useState } from "react";
import { ThemeContext } from "../../store/themeContext/themeContext";
import styles from "./createAccountStyles.module.css";

export interface Props {
    setUsername: any;
    setPassword: any;
    setInvite: any;
    setMnemonic: any;
    password: any;
    username: string;
    invite: any;
    createAccount: any;
}

function CreateAccount(props: Props) {

    const [passwordMatch, setPasswordMatch] = useState(false);

    const [password, setPassword] = useState("");
    const [invite, setInvite] = useState("");
    const handlePassword = (e: any) => {
      props.setPassword(e.target.value);
    };
    const handleInvite = (e: any) => {
      props.setInvite(e.target.value);
      props.setMnemonic(e.target.value.split(" "));
    };
  
    return (
      <div className={styles.formcontainer}>
       
        <div className={styles.title}>Connect with GetLoginETH</div>
        <div className={styles.passwordflex} />
        <div className={styles.flexer} />
        <div className={styles.usernameinputbox}>
          <input
            type="text"
            autoFocus={true}
            className={styles.getLoginUsernameInput}
            placeholder="Username"
            value={props.username}
            onChange={(e) => props.setUsername(e.target.value)}
          />
        </div>
        <div className={styles.usernameinputbox}>
          <input
            type="password"
            autoFocus
            name="1"
            className={styles.mnemonicinput}
            placeholder="Password"
            value={props.password}
            onChange={(e) => handlePassword(e)}
          />
        </div>
        <div className={styles.usernameinputbox}>
          <input
            type="text"
            name="2"
            className={styles.mnemonicinput}
            placeholder="GetLogin Invite"
            value={props.invite}
            onChange={(e) => handleInvite(e)}
          />
        </div>
  
        <div className={styles.button} onClick={props.createAccount}>
          <div>
            <div className={styles.buttontext}>
              Login with GetLoginETH account
            </div>
          </div>
        </div>
      </div>
    );
}


export default React.memo(CreateAccount);
