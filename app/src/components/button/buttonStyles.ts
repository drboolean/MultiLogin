import { makeStyles, createStyles } from "@material-ui/core/styles";
import { Theme } from "../../store/themeContext/themes";
import { Props } from "./button";

const useStyles = makeStyles(() =>
  createStyles({
    button: {
      width: "100%",
      height: "100%", 
      borderRadius: "10rem 0 0 10rem",
    },
    wrap: {
      width: "99.3%",
      height: "100%",
      alignSelf: "left",
      borderRadius: "10rem 0 0 10rem",
      border: `1px solid black`,
    },
  })
);

export default useStyles;
