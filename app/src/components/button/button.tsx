import React, { useContext } from "react";
import { ThemeContext } from "../../store/themeContext/themeContext";
import useStyles from "./buttonStyles";

export interface Props {

}

function Button(props: Props) {
  const { theme } = useContext(ThemeContext);

  const classes = useStyles({ ...props, ...theme });

  return (
    <div className={classes.button}>
      <button className={classes.wrap}>
        Submit
      </button>
    </div>
  );
}

export default React.memo(Button);
