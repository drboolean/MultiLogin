import React, { useContext, useState } from "react";
import { ThemeContext } from "../../store/themeContext/themeContext";
import useStyles from "./inputStyles";

export interface Props {
  title: string;
}

function Input(props: Props) {
  const { theme } = useContext(ThemeContext);
  const [ textInput, setTextInput ] = useState("");
  const classes = useStyles({ ...props, ...theme });

  const handleChange = (e: any) => {
    setTextInput(e.target.value)
  }

  return (
    <div className={classes.Input}>
      <div className={classes.heading}>{props.title}</div>
      <input
        className={classes.input}
        type="text"
        value={textInput}
        onChange={handleChange}
      />
    </div>
  );
}

export default React.memo(Input);
