import React from "react";
import "./App.css";
import { StoreProvider } from "./store/store";
import { ThemeProvider } from "./store/themeContext/themeContext";
import Main  from "./containers/main/main";



const App = () => {
  return (
    <div className="App">
      <StoreProvider>
        <ThemeProvider>
         <Main></Main>
        </ThemeProvider>
      </StoreProvider>
    </div>
  );
};

export default App;
