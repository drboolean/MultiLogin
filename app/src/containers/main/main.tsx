import { ethers } from "ethers";
import React, { useContext } from "react";
import Web3 from "web3";
import { StoreContext } from "../../store/store";
import CreateAccount from "../../components/createAccount/createAccount";
import GetLoginLogic from "../../interfaces/GetLoginLogic.json";
import GetLoginStorage from "../../interfaces/GetLoginStorage.json";
import { useState } from "react";


export interface Props {
}

function Main(props: Props) {
  const { state } = useContext(StoreContext);
  const [mnemonic, setMnemonic] = useState([]);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState();
  const [invite, setInvite] = useState("");

  async function handleUsername(username: string) {
    setUsername(username);
  }

  const createAccountWithGetLoginConnect = async () => {
    debugger;
    const Web3Provider = new Web3(
      new Web3.providers.HttpProvider(
        `${process.env.REACT_APP_GOERLI_ENDPOINT}`
      )
    );
    const inviteWallet = ethers.Wallet.fromMnemonic(invite);
    const wallet = Web3Provider.eth.accounts.create();
    const encryptedWallet = encryptWallet(wallet, password);

    const logicContract = new Web3Provider.eth.Contract(
        //@ts-ignore
      GetLoginLogic.abi,
      process.env.REACT_APP_LOGIC,
      {
        from: inviteWallet.address,
      }
    );
    const storageContract = new Web3Provider.eth.Contract(
        //@ts-ignore
      GetLoginStorage.abi,
      process.env.REACT_APP_STORAGE
    );
    const usernameHash = getUsernameHash(username);
    const info = await logicContract.methods
      .createUserFromInvite(
        usernameHash,
        "0x" + encryptedWallet.address,
        encryptedWallet.crypto.ciphertext,
        encryptedWallet.crypto.cipherparams.iv,
        encryptedWallet.crypto.kdfparams.salt,
        encryptedWallet.crypto.mac,
        true
      )
      .encodeABI();
    let result = {
      from: inviteWallet.address,
      to: process.env.REACT_APP_LOGIC,
      value: Web3.utils.toWei("0.1", "ether"),
      data: info,
    };
    const signedTx = await signAndSendTx(
      result,
      inviteWallet.privateKey,
      Web3Provider
    );
    debugger;
    console.log(signedTx)
  };
  const signAndSendTx = async (
    result: any,
    privateKey: string,
    Web3Provider: Web3
  ) => {
    const gasPrice = 2;
    const gasLimit = 3000000;
    result.gasLimit = Web3.utils.toHex(gasLimit);
    result.gasPrice = Web3.utils.toHex(gasPrice * 1e9);
    let signed: any;
    signed = await Web3Provider.eth.accounts.signTransaction(
      result,
      privateKey
    );
    return await Web3Provider.eth.sendSignedTransaction(signed.rawTransaction);
  };

  const encryptWallet = (wallet: any, password: any) => {
    return wallet.encrypt(password);
  };
  const filterUsername = (username: any) => {
    return username.trim();
  };

  const getUsernameHash = (username: any) => {
    username = filterUsername(username);

    return Web3.utils.keccak256(username);
  };



  return (
 <CreateAccount 
 username={username}
 setUsername={handleUsername}
 setPassword={setPassword}
 password={password}
 setInvite={setInvite}
 setMnemonic={setMnemonic}
 invite={invite}
 createAccount={createAccountWithGetLoginConnect}
 
 ></CreateAccount>
  );
}

export default React.memo(Main);
