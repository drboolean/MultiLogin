import { ethers } from 'hardhat';
async function main() {
  const [deployer] = await ethers.getSigners();
  // tslint:disable-next-line: no-console
  console.log(
      "Deploying contracts with the account:",
      deployer.address
    );
    // console.log("Account balance:", (await deployer.getBalance()).toString());
    const Token = await ethers.getContractFactory("Token");
    const token = await Token.deploy();
    // tslint:disable-next-line: no-console
    console.log("Token address:", token.address);
  }

  main()
    .then(() => process.exit(0))
    .catch(error => {
      // tslint:disable-next-line: no-console
      console.error(error);
      process.exit(1);
    });