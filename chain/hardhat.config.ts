import { task } from "hardhat/config";
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-waffle";
import { HardhatUserConfig } from 'hardhat/types';
import * as dotenv from "dotenv";

dotenv.config();


/**
 * @type import('hardhat/config').HardhatUserConfig
 */

const config: HardhatUserConfig = {
  defaultNetwork: "localhost",
  solidity: {
    compilers: [{ version: "0.7.3", settings: {} }],
  },
  networks: {
    localhost: {
      url: "http://localhost:8545",
      chainId: 31337
    },
    ropsten: {
      url: `https://rinkeby.infura.io/v3/${process.env.INFURA_PROJECT_ID}`,
      accounts: [`0x${process.env.ROPSTEN_PRIVATE_KEY}`],
    },
  },
  // mocha options can be set here
  mocha: {
    // timeout: "300s",
  },
};
export default config;