
# Warp Core Boilerplate!

Welcome to our React/blockchain boiler plate application.

## Technology stack

This application uses Typescript, React, Hooks, Hardhat.

## Project Setup

  

### Install dependencies

  

Inside chain folder run 
```
yarn install
```
which will install all required dependencies for chain app.
Inside app folder run the same command  ```
yarn install
``` which will install all required dependencies for react app.

### WARNING!
Before you start the project you either need to import infura key and private key to your .env file or comment out ropsten inside hardhat config file the following: 
```
ropsten: {
	url: https://rinkeby.infura.io/v3/${process.env.INFURA_PROJECT_ID},
	accounts: [0x${process.env.DEPLOYER_PRIVATE_KEY_ROPSTEN}],
},
```

### Start project
#### Starting chain project
After successful installation of dependencies, run  ``` yarn start``` to start chain devnet.
Inside .env file copy the private key from the first address, that will be your deployer address.
After starting the chain locally and adding the deployer address you can deploy your contracts using command: ```yarn deploy:local```

#### Starting app project
When you have done that copy addresses that were given to you from deployed contracts to React ENV. You can find example under /app/.env.example.
And run command ```
yarn start
```

  

### Project structure
In the project structure inside chain directory we have the following parts:

```
|-app
	|- public/
	|- src/
		|- components/
		|- containers/
		|- interfaces/
		|- store/
			|- services/
			|- themeContext/
  
|-chain
	|- contracts/
	|- scripts/
	|- test/
	|- typings/
```
app folder: 
	public folder contains Title and icon for your application
	Under src folder is the logic: 
		directories:
			interfaces folder contains deployed contracts
			store folder contains services and theme context
				services folder contains all services

chain folder:
	contract folder contains written contracts for this chain.
	scripts folder contains file for deployment
	test folder contains files for each written contract and full coverage tests
	typings folder contains file for loading js modules such as dotenv


## Testing

Running `yarn run test` will start test for contracts.

Hardhat's testing framework is built on Waffle, which is built on Chai, which is built on Mocha. If you need to alter Mocha defaults, this can be done through the `mocha` object in `hardhat.config.ts`. A common example is the test timeout - Mocha defaults to 6 seconds, and tests can need longer. A commented out line appears in the `mocha` object for this. Uncommenting it will move the timeout to five minutes, which may be too long for your needs, so edit as you see fit.